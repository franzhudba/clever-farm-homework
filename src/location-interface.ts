export interface ILocation {
  id: string;
  name: string;
  description: string;
  coordinates: readonly number[];
}
