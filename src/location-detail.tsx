import { LocationMap } from "./map";
import { useParams } from "react-router";
import { Typography, Grid, Button } from "@mui/material";
import { useLocation } from "./useLocation";
import { useTranslation } from "react-i18next";
import { ILocation } from "./location-interface";
import { Link } from "react-router-dom";

export function LocationDetail(): JSX.Element {
  const { id } = useParams<{ id: string }>();
  const location: ILocation = useLocation(id);
  const { t } = useTranslation();

  return (
    <Grid container direction="column" alignItems="center" minHeight="100vh">
      <Grid item margin={2}>
        <Typography variant="h5" component="div">
          {t("name")}: {location.name}
        </Typography>
        <Typography component="div" mb={2}>
          {t("coordinates")}: {location.coordinates.join(", ")}
        </Typography>
        <Link to="/locations" style={{ float: "right" }}>
          <Button variant="contained" size="medium">
            {t("locations")}
          </Button>
        </Link>
      </Grid>
      <Grid item container flexGrow={2}>
        <LocationMap
          coordinates={location.coordinates}
          name={location.name}
          description={location.description}
          locationId={location.id}
        />
      </Grid>
    </Grid>
  );
}
