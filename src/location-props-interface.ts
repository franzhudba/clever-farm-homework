export interface ILocationProps {
  locationId: string;
  name: string;
  description: string;
  coordinates: readonly number[];
}
