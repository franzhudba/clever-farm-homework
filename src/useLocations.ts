import { useDispatch, useSelector } from "react-redux";
import { loadLocations } from "./action-creators/load-locations";
import { ILocation } from "./location-interface";
import { getAll } from "./location-service";

export function useLocations(): ILocation[] {
  const locations: ILocation[] | null = useSelector<
    { locations: ILocation[] | null },
    ILocation[] | null
  >((state) => state.locations);
  const dispatch = useDispatch();
  if (!locations) {
    const response = getAll();
    dispatch(loadLocations(response));
    return response;
  }
  return locations;
}
