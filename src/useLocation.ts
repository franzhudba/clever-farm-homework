import { useDispatch, useSelector } from "react-redux";
import { loadLocations } from "./action-creators/load-locations";
import { ILocation } from "./location-interface";
import { getAll } from "./location-service";

function findLocation(
  locations: ILocation[],
  id: string
): ILocation | undefined {
  return locations.find((location: ILocation) => {
    return location.id === id;
  });
}

export function useLocation(id: string): ILocation {
  const locations: ILocation[] | null = useSelector<
    { locations: ILocation[] | null },
    ILocation[] | null
  >((state) => state.locations);
  const dispatch = useDispatch();
  let location: ILocation | undefined;
  if (locations) {
    location = findLocation(locations, id);
  }
  if (!locations) {
    const response = getAll();
    dispatch(loadLocations(response));
    location = findLocation(response, id);
  }
  if (location) {
    return location;
  }
  throw new Error(`Location with id ${id} was not found`);
}
