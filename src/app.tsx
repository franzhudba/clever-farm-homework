import { StrictMode } from "react";
import { render } from "react-dom";
import { LocationDetail } from "./location-detail";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Container } from "@mui/material";
import { Provider } from "react-redux";
import store from "./store";
import { Locations } from "./locations";
import "./i18n";

export function App(): JSX.Element {
  return (
    <div>
      <Router>
        <Switch>
          <Route path="/location/:id">
            <Container maxWidth={false} disableGutters={true}>
              <LocationDetail />
            </Container>
          </Route>
          <Route path="/">
            <Locations />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

render(
  <StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </StrictMode>,
  document.getElementById("root")
);
