import { Card } from "./card";
import { Grid } from "@mui/material";
import { ILocation } from "./location-interface";
import { useLocations } from "./useLocations";
import { Typography, Container } from "@mui/material";
import { useTranslation } from "react-i18next";

export function Locations(): JSX.Element {
  const locations: ILocation[] = useLocations();
  const { t } = useTranslation();

  return (
    <Container maxWidth={false} disableGutters={true}>
      <Typography
        sx={{
          textAlign: "center",
          borderBottom: "1px solid #eee",
          margin: "40px 0 10px 0",
        }}
        variant="h3"
        component="div"
      >
        {t("locations")}
      </Typography>
      <Grid container justifyContent="center">
        <Grid container item spacing={2} maxWidth="lg" justifyContent="center">
          {locations.map((location) => {
            return (
              <Grid item xs={12} sm={6} md={4} key={location.id}>
                <Card
                  name={location.name}
                  locationId={location.id}
                  description={location.description}
                  coordinates={location.coordinates}
                />
              </Grid>
            );
          })}
        </Grid>
      </Grid>
    </Container>
  );
}
