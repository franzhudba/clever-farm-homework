import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import LanguageDetector from "i18next-browser-languagedetector";

const resources = {
  en: {
    translation: {
      name: "Name",
      description: "Description",
      detail: "Detail",
      coordinates: "Coordinates",
      locations: "Locations",
    },
  },
  cs: {
    translation: {
      name: "Název",
      description: "Popis",
      detail: "Detail",
      coordinates: "Souřadnice",
      locations: "Lokality",
    },
  },
};

void i18n
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    resources,
    fallbackLng: "en",
    // lng: "en",
    interpolation: {
      escapeValue: false,
    },
  });

export default i18n;
