import { ILocation } from "../location-interface";

export interface ILoadLocationsAction {
  type: string;
  payload: ILocation[];
}

export function loadLocations(locations: ILocation[]): ILoadLocationsAction {
  return { type: "LOAD_LOCATIONS", payload: locations };
}
