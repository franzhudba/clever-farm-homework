import {
  Card as MUICard,
  CardContent,
  Typography,
  CardActions,
  Button,
} from "@mui/material";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { ILocationProps } from "./location-props-interface";

export function Card(props: ILocationProps): JSX.Element {
  const { t } = useTranslation();
  return (
    <MUICard
      sx={{
        minWidth: 275,
        minHeight: 210,
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-between",
      }}
    >
      <CardContent>
        <Typography variant="h5" component="div">
          {t("name")}: {props.name}
        </Typography>
        <Typography component="div" mb={2}>
          {t("description")}: {props.description}
        </Typography>
      </CardContent>
      <CardActions sx={{ justifyContent: "flex-end" }}>
        <Link to={`/location/${props.locationId}`}>
          <Button variant="contained" size="medium">
            {t("detail")}
          </Button>
        </Link>
      </CardActions>
    </MUICard>
  );
}
