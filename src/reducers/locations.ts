import { ILoadLocationsAction } from "../action-creators/load-locations";
import { ILocation } from "../location-interface";

export function locations(
  state: ILocation[] | null = null,
  action: ILoadLocationsAction
): ILocation[] | null {
  switch (action.type) {
    case "LOAD_LOCATIONS":
      return action.payload;
    default:
      return state;
  }
}
