import React, { useRef, useEffect, MutableRefObject } from "react";
import Map from "ol/Map";
import View from "ol/View";
import TileLayer from "ol/layer/Tile";
import VectorLayer from "ol/layer/Vector";
import VectorSource from "ol/source/Vector";
import { fromLonLat } from "ol/proj";
import OSM from "ol/source/OSM";
import Point from "ol/geom/Point";
import { ILocationProps } from "./location-props-interface";
import Style from "ol/style/Style";
import { Circle } from "ol/style";
import Fill from "ol/style/Fill";
import { Feature } from "ol";

export function LocationMap(props: ILocationProps): JSX.Element {
  const mapElement: MutableRefObject<HTMLDivElement | null> = useRef(null);

  useEffect(() => {
    const feature = new Feature({
      geometry: new Point(fromLonLat([...props.coordinates])),
    });
    const points = new VectorLayer({
      source: new VectorSource({
        features: [feature],
      }),
      style: new Style({
        image: new Circle({
          radius: 9,
          fill: new Fill({ color: "green" }),
        }),
      }),
    });

    new Map({
      target: mapElement.current ? mapElement.current : undefined,
      layers: [
        new TileLayer({
          source: new OSM(),
        }),
        points,
      ],
      view: new View({
        center: fromLonLat([...props.coordinates]),
        zoom: 12,
      }),
      controls: [],
    });
  }, [props.coordinates]);

  return (
    <div
      ref={mapElement}
      data-testid="map-container"
      className="map-container"
    ></div>
  );
}
